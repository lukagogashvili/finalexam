package com.example.registrationpage

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth

class ChangePasswordActivity : AppCompatActivity() {


    private lateinit var editTextNewPassword : EditText
    private lateinit var editTextNewPassword2 : EditText
    private lateinit var buttonChangepassword : Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_change_password)
        init()
        changepassword()
    }

    private fun init() {
        editTextNewPassword = findViewById(R.id.editTextNewPassword)
        editTextNewPassword2 = findViewById(R.id.editTextNewPassword2)
        buttonChangepassword = findViewById(R.id.buttonChangepassword)
    }
    private fun changepassword() {
        buttonChangepassword.setOnClickListener {

            val newPassword = editTextNewPassword.text.toString()
            val newPassword1 = editTextNewPassword2.text.toString()

            if (newPassword.isEmpty() || newPassword1.isEmpty()) {
                Toast.makeText(this, "Fill all the fields!!", Toast.LENGTH_SHORT).show()
            }
            if (newPassword != newPassword1) {
                Toast.makeText(this, "Passwords do not match!!", Toast.LENGTH_SHORT).show()
            }
            FirebaseAuth.getInstance().currentUser?.updatePassword(newPassword)?.addOnCompleteListener { task ->
                if (task.isSuccessful) {
                    Toast.makeText(this, "Your password has been changed !!", Toast.LENGTH_SHORT).show()
                }else
                    Toast.makeText(this, "Error!!", Toast.LENGTH_SHORT).show()
            }
        }
    }
}