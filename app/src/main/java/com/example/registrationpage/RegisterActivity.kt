package com.example.registrationpage

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.CheckBox
import android.widget.EditText
import android.widget.Toast
import com.google.firebase.auth.FirebaseAuth

class RegisterActivity : AppCompatActivity() {


    private lateinit var password : EditText
    private lateinit var password2 : EditText
    private lateinit var emailAddress : EditText
    private lateinit var buttonSignup : Button
    private lateinit var chekBox : CheckBox
    private lateinit var buttonSignin : Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_register)
        init()
        registerListener()
        loginListener()
    }

    private fun init() {

        password = findViewById(R.id.password)
        password2 = findViewById(R.id.password2)
        emailAddress = findViewById(R.id.emailAddress)
        buttonSignup = findViewById(R.id.buttonSignup)
        chekBox = findViewById(R.id.checkBox)
        buttonSignin = findViewById(R.id.buttonSignin)
    }

    private fun registerListener() {
        buttonSignup.setOnClickListener {
            val email = emailAddress.text.toString()
            val password = password.text.toString()
            val password2 = password2.text.toString()

            if (email.isEmpty() || password.isEmpty() || password2.isEmpty() || !chekBox.isChecked) {
                Toast.makeText(this, "Fill in all the fields!!", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            if (!email.contains("@")){
                Toast.makeText(this, "E-mail is incorrect!", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            if (password != password2){
                Toast.makeText(this, "Passwords are not match!", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            if (password.length < 9) {
                Toast.makeText(this, "Password should be more than 9 symbols !!", Toast.LENGTH_SHORT).show()
                return@setOnClickListener
            }
            FirebaseAuth.getInstance().createUserWithEmailAndPassword(email, password)
                .addOnCompleteListener { task ->
                    if (task.isSuccessful) {
                        Toast.makeText(this, "Congratulations you have been registered!!", Toast.LENGTH_SHORT).show()
                    }
                }

        }
    }
    private fun loginListener() {
        buttonSignin.setOnClickListener {
            val intent = Intent(this, LoginActivity::class.java)
            startActivity(intent)
            return@setOnClickListener
        }
    }
}